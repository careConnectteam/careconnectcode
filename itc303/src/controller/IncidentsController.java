package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class IncidentsController extends Controller{

    @FXML Label labelUserName = new Label();
    @FXML Label labelLogOut = new Label();
    @FXML TextField tfSearch = new TextField();
    @FXML Button btnSearch = new Button();
    @FXML Button btnPendingIncidents = new Button();
    @FXML Button btnReportLog = new Button();
    @FXML Button btnCreateIncident = new Button();
    @FXML Button btnHome = new Button();
    @FXML Button btnIncident = new Button();
    @FXML AnchorPane justAdmin = new AnchorPane();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        labelUserName.setText(currentUser.getUsername());
        justAdmin.setVisible(false);

        if(currentUser.getType().equals("manager")){
            justAdmin.setVisible(true);
        }

        btnHome.setOnAction(event -> {
            goToDashboard(btnHome);
        });

        setupNavListener(labelLogOut, "login");
        setupNavListener(btnIncident, "incidents");
        setupNavListener(btnPendingIncidents, "pendingIncidents");
        setupNavListener(btnCreateIncident, "createIncident");

        btnSearch.setOnAction(event -> {
            keyword = tfSearch.getText();
            goToScreen(btnSearch, "incidentSearchResults");
        });
    }
}
