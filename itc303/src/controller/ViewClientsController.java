package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import model.Client;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ViewClientsController extends Controller{

    @FXML private TableView clientsTable = new TableView();
    @FXML private GridPane clientView = new GridPane();
    @FXML private Label labelUserName = new Label();
    @FXML private Button btnHome = new Button();
    @FXML private Button btnClients = new Button();

    private ObservableList<Client> clientsList = FXCollections.observableArrayList();//a list that contains the client objects

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        labelUserName.setText(currentUser.getUsername());

        setupTable();
        setUpTableListener();
        listClients();
        setupNavListener(btnHome, "dashboard");
        setupNavListener(btnClients, "clients");
    }

    private void setupTable(){
        TableColumn colId = new TableColumn("Client Id");
        TableColumn colName = new TableColumn("Name");
        TableColumn colDob = new TableColumn("Date of Birth");
        TableColumn colGName = new TableColumn("Guardian Name");
        TableColumn colGContact = new TableColumn("Guardian Contact");

        clientsTable.getColumns().addAll(colId, colName, colDob, colGName, colGContact);

        colId.prefWidthProperty().bind(clientsTable.widthProperty().multiply(0.2));
        colId.prefWidthProperty().bind(clientsTable.widthProperty().multiply(0.2));
        colDob.prefWidthProperty().bind(clientsTable.widthProperty().multiply(0.2));
        colGName.prefWidthProperty().bind(clientsTable.widthProperty().multiply(0.2));
        colGContact.prefWidthProperty().bind(clientsTable.widthProperty().multiply(0.2));

        colId.setCellValueFactory(new PropertyValueFactory<Client, String>("clientId"));
        colId.setCellValueFactory(new PropertyValueFactory<Client, String>("name"));
        colDob.setCellValueFactory(new PropertyValueFactory<Client, String>("dob"));
        colGName.setCellValueFactory(new PropertyValueFactory<Client, String>("guardianName"));
        colGContact.setCellValueFactory(new PropertyValueFactory<Client, String>("guardianContact"));
    }

    private void setUpTableListener(){//double clicking on each row will show the details on the gridpane on bottom
        clientsTable.setRowFactory( tv -> {
            TableRow<Client> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Client rowData = row.getItem();
                    viewClient(rowData);
                }
            });
            return row ;
        });
    }

    private void viewClient(Client cl){
        clientView.getChildren().clear();

        Label lId = new Label("Client ID: ");
        Label lIdValue = new Label(String.valueOf(cl.getClientId()));

        Label lName = new Label("Client Name: ");
        Label lNameValue = new Label(cl.getName());

        Label lDob = new Label("Date of Birth: ");
        Label lDobValue = new Label(cl.getDob());

        Label lGuardian = new Label("Guardian Name: ");
        Label lGuardianValue = new Label(cl.getGuardianName());

        Label lGuardianContact = new Label("Guardian Contact");
        Label lGuardianContactValue = new Label(cl.getGuardianContact());

        clientView.add(lId, 0, 0);
        clientView.add(lIdValue, 1, 0);
        clientView.add(lName, 0, 1);
        clientView.add(lNameValue, 1, 1);
        clientView.add(lDob, 0, 2);
        clientView.add(lDobValue, 1, 2);
        clientView.add(lGuardian, 0, 3);
        clientView.add(lGuardianValue, 1, 3);
        clientView.add(lGuardianContact, 0, 4);
        clientView.add(lGuardianContactValue, 1, 4);

        clientView.setVgap(20);
        clientView.setHgap(20);
    }

    private void listClients(){//adds data and displays on the tableview.
        clientsTable.getItems().clear();

        JSONObject clientsJson = null;
        try {
            clientsJson = httpConnector.post("getClients.php");

            JSONArray jsonArray = clientsJson.getJSONArray("clients");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject j = jsonArray.getJSONObject(i);
                Client cl = new Client(
                        j.getInt("cl_id"),
                        j.getString("cl_name"),
                        j.getString("cl_dob"),
                        j.getString("cl_guardian_name"),
                        j.getString("cl_guardian_contact"));
                clientsList.add(cl);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        clientsTable.setItems(clientsList);//list is added to TableView
    }
}
