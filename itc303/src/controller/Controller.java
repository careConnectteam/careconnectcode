package controller;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Manager;
import model.Staff;
import model.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public abstract class Controller implements Initializable {
    //this class is created because almost all controller need to
    // keep changing screens and the codes for changing screens is reused
    //several times. after extending this class they will all inherit the
    //common behaviour of going to a different screen. now all they need to do is call
    //a method

    static User currentUser = null;//will be initialized later
    static String keyword = "";
    HttpConnector httpConnector = new HttpConnector();

    void goToDashboard(Node context){
        goToScreen(context, "dashboard");
    }

    void goToScreen(Node context, String fxmlFile){//context is not really context but the component in the current gives idea of where it is currently in
        try {
            AnchorPane body = FXMLLoader.load(getClass().getResource("../view/" + fxmlFile + ".fxml"));

            //load the screen
            Stage newStage = (Stage) context.getScene().getWindow();
            newStage.setScene(new Scene(body));
            newStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    User getCurrentUser(String id){
        User user = null;
        JSONObject resultObject = null;
        try {
            resultObject = httpConnector.postWithParams("getUser.php",  "s_id=" + id);
            JSONArray jArray = resultObject.getJSONArray("user");
            JSONObject jsonUser = jArray.getJSONObject(0);

            if(jsonUser.getString("s_type").equals("manager")){
                user = new Manager(jsonUser.getString("s_id"), jsonUser.getString("s_name"), jsonUser.getString("s_contact"), jsonUser.getString("s_address"), jsonUser.getString("s_type"), jsonUser.getString("username"));
            }
            else {
                user = new Staff(jsonUser.getString("s_id"), jsonUser.getString("s_name"), jsonUser.getString("s_contact"), jsonUser.getString("s_address"), jsonUser.getString("s_type"), jsonUser.getString("manager"), jsonUser.getString("username") );
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return user;
    }

    Staff getStaff(String id){
        Staff staff = null;
        JSONObject resultObject;
        try {
            resultObject = httpConnector.postWithParams("getUser.php",  "s_id=" + id);
            JSONArray jArray = resultObject.getJSONArray("user");
            JSONObject jsonUser;
            for(int i = 0; i<jArray.length(); i++){
                jsonUser = jArray.getJSONObject(i);
                if(jsonUser.getString("s_type").equals("staff")){
                    staff = new Staff(jsonUser.getString("s_id"), jsonUser.getString("s_name"), jsonUser.getString("s_contact"), jsonUser.getString("s_address"), jsonUser.getString("s_type"), jsonUser.getString("manager"), jsonUser.getString("username") );
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return staff;
    }

    Manager getManager(String id){
        Manager manager = null;
        JSONObject resultObject;
        try {
            resultObject = httpConnector.postWithParams("getUser.php",  "s_id=" + id);
            JSONArray jArray = resultObject.getJSONArray("user");
            JSONObject jsonUser;
            for(int i = 0; i<jArray.length(); i++){
                jsonUser = jArray.getJSONObject(i);if(jsonUser.getString("s_type").equals("manager")){
                    manager = new Manager(jsonUser.getString("s_id"), jsonUser.getString("s_name"), jsonUser.getString("s_contact"), jsonUser.getString("s_address"), jsonUser.getString("s_type"), jsonUser.getString("username"));
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return manager;
    }

    void setupNavListener(Button node, String fxmlFile) {
        node.setOnAction(event -> {
            goToScreen(node, fxmlFile);//from parent class Controller
        });
    }
    void setupNavListener(Label node, String fxmlFile) {
        node.setOnMouseClicked(event -> {
            goToScreen(node, fxmlFile);//from parent class Controller
        });
    }

    void logOut(){
        currentUser = null;
        keyword = "";
    }


}
