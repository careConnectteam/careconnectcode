package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.json.JSONObject;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.scene.control.*;

public class LoginController extends Controller{

    @FXML TextField tfUserName = new TextField();
    @FXML PasswordField pfPasswordField = new PasswordField();
    @FXML Button btnLogin = new Button();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setupLoginButtonListener();
    }

    public void setupLoginButtonListener() {
        btnLogin.setOnAction(event -> {
            logOut();
            String userID  = login(tfUserName.getText(), pfPasswordField.getText());

            if(!userID.equals("-1")){//if login unsuccessful
                goToDashboard(btnLogin);
            }
            else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Invalid or Mismatching Credentials! Try Again! ", ButtonType.OK);
                alert.showAndWait();

                if (alert.getResult() == ButtonType.OK) {
                    setupLoginButtonListener();

                    System.out.println("login failed");}
            }
        });
    }

    private String login(String userName, String password) {
        //still need to implement login logic
        HttpConnector login = new HttpConnector();
        JSONObject loginStatus;
        String loggedIn = null;
        try {
            loginStatus = login.postWithParams("login.php", "username=" + userName + "&password=" + password);
            loggedIn = loginStatus.get("user").toString();
            currentUser = getCurrentUser(loggedIn);
        }
        catch (Exception e)
        {
            System.out.println("Exception occured in login function: "+ e.toString());
        }

        return loggedIn;
    }
}
