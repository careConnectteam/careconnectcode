package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Client;
import model.Incident;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ClientSearchResultsController extends Controller {

    @FXML TextField tfSearch = new TextField();
    @FXML Button btnSearch = new Button();

    private Client resultClient = null;

    @FXML private TableView incidentsTable = new TableView();

    private ObservableList<Incident> unapprovedIncidentsList = FXCollections.observableArrayList();//a list that contains the student objects

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btnSearch.setOnAction(event -> {
            if(tfSearch.getText() == null){
                System.out.println("Please enter something");
            }else {
                resultClient = searchClient(tfSearch.getText());
            }
        });
    }

    private Client searchClient(String keyword){
        Client cl = null;
        try {
            JSONObject result = httpConnector.postWithParams("clientSearch.php", "key=" + keyword);
            JSONObject clientJson = result.getJSONObject("clients");
            cl = new Client(clientJson.getInt("cl_id"),
                    clientJson.getString("cl_name"),
                    clientJson.getString("cl_dob"),
                    clientJson.getString("cl_guardian_name"),
                    clientJson.getString("cl_guardian_contact"));
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return cl;
    }
}
