package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import java.net.URL;
import java.util.ResourceBundle;

public class DashboardController extends Controller implements Initializable {

    @FXML Label labelUserName = new Label();
    @FXML Label labelFullName = new Label();
    @FXML Label labelLogOut = new Label();

    @FXML Button btnNotes = new Button();
    @FXML Button btnClients = new Button();
    @FXML Button btnIncidents = new Button();


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        setUpListeners();
    }

    public void setUpListeners(){
        labelUserName.setText(currentUser.getUsername());
        labelFullName.setText(currentUser.getName());

        setupNavListener(labelLogOut, "login");
        setupNavListener(btnNotes, "notes");
        setupNavListener(btnClients, "clients");
        setupNavListener(btnIncidents, "incidents");

    }


}
