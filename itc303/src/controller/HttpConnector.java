package controller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpConnector{
    private static final String MAIN_URL = "http://auslanpedia.online/";
    //private static final String MAIN_URL = "http://192.168.1.26/careconnect/";
    HttpURLConnection con;

    public JSONObject post(String php) throws IOException {
        JSONObject jsonObject = null;

        String urlTxt = MAIN_URL + php;

        URL url = new URL(urlTxt);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");

        int responseCode = con.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            try {
                jsonObject = new JSONObject(response.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            System.out.println("POST request not worked");
        }

        return jsonObject;
    }

    public JSONObject postWithParams(String php, String params) throws IOException {
        JSONObject jsonObject = null;

        String urlTxt = MAIN_URL + php;

        URL url = new URL(urlTxt);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");

        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(params.getBytes());
        os.flush();
        os.close();

        int responseCode = con.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            try {
                jsonObject = new JSONObject(response.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            System.out.println("POST request not worked");
        }

        return jsonObject;
    }

    public void postNoJSON(String php, String params) throws IOException {
        String urlTxt = MAIN_URL + php;

        URL url = new URL(urlTxt);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");

        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(params.getBytes());
        os.flush();
        os.close();

        int responseCode = con.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) { //success

        } else {
            System.out.println("POST request not worked");
        }
    }


}
