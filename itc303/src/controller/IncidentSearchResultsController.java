package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import model.Client;
import model.Incident;
import model.Manager;
import model.Staff;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class IncidentSearchResultsController extends Controller{

    @FXML private TextField tfKeyword = new TextField();
    @FXML private Button btnHome = new Button();
    @FXML private Button btnIncident = new Button();
    @FXML private Button btnSearch = new Button();
    @FXML Label labelUserName = new Label();
    @FXML Label labelKeyword = new Label();
    @FXML Label labelLogOut = new Label();
    @FXML HBox crud = new HBox();
    @FXML HBox msgBox = new HBox();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setUp();
        onlyAdmin();
    }

    private void setUp(){
        setupNavListener(btnHome, "dashboard");
        setupNavListener(btnIncident, "incidents");
        setupNavListener(labelLogOut, "login");

        labelUserName.setText(currentUser.getUsername());
        crud.setVisible(false);

        btnSearch.setOnAction(event -> {
            labelKeyword.setText(tfKeyword.getText());
            if(tfKeyword.getText().equals("")){
                error();
            }else{
                Incident in = getIncident(tfKeyword.getText());
                if(in != null){
                    displayIncident(in);
                }else{
                    error();
                }
            }
        });
        labelKeyword.setText(keyword);
        Incident in = getIncident(keyword);
        if(in != null){
            displayIncident(in);
        }else{
            error();
        }

    }

    private void onlyAdmin(){
        if(currentUser.getType().equals("manager")){
            Button btnEdit = new Button();
            Button btnDelete = new Button();

            btnEdit.setOnAction(event -> {
                try {
                    httpConnector.postNoJSON("editIncident.php", "s_id=" + currentUser.getUserId());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            btnDelete.setOnAction(event -> {
                try {
                    httpConnector.postNoJSON("deleteIncident.php", "s_id=" + currentUser.getUserId());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            crud.getChildren().addAll(btnEdit, btnDelete);
            crud.setVisible(true);
        }
    }

    private Incident getIncident(String keyword){
        Incident in = null;
        try {
            JSONObject jsonResult = httpConnector.postWithParams("getIncident.php", "in_id=" + keyword);
            System.out.println(jsonResult.toString());
            JSONArray jsonArray = jsonResult.getJSONArray("incident");
            System.out.println(jsonArray.toString());
            JSONObject jsonIncident = null;
            if(jsonArray.length() != 0){
                jsonIncident = jsonArray.getJSONObject(0);
                in = new Incident(
                        Integer.valueOf(jsonIncident.getString("in_id")),
                        jsonIncident.getString("in_time"),
                        jsonIncident.getString("in_date"),
                        jsonIncident.getString("in_type"),
                        jsonIncident.getString("in_detail"),
                        jsonIncident.getString("is_approved"),
                        jsonIncident.getString("client"),
                        jsonIncident.getString("staff"),
                        jsonIncident.getString("manager")
                );
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return in;
    }

    private void displayIncident(Incident in){
        msgBox.getChildren().clear();

        GridPane incidentView = new GridPane();
        Client client = getClient(in.getClientId());
        Staff staff = getStaff(in.getStaffId());
        Manager manager = getManager(in.getManagerId());

        Label labelIncidentId = new Label();
        Label labelManager = new Label();
        Label labelClient = new Label();
        Label labelType = new Label();
        Label labelDate = new Label();
        Label labelTime = new Label();
        Label labelStaff = new Label();
        Label labelDescription = new Label();

        labelIncidentId.setText(String.valueOf(in.getIncidentId()));
        labelManager.setText(manager.getName());
        labelClient.setText(client.getName());
        labelType.setText(in.getType());
        labelDate.setText(in.getIncidentDate());
        labelTime.setText(in.getIncidentTime());
        labelStaff.setText(staff.getName());
        labelDescription.setText(in.getDetail());

        incidentView.add(new Label("Incident Id: "), 0, 0);
        incidentView.add(labelIncidentId, 1, 0);
        incidentView.add(new Label("Manager"), 0, 1);
        incidentView.add(labelManager, 1, 1);
        incidentView.add(new Label("Client Involved: "), 0, 2);
        incidentView.add(labelClient, 1, 2);
        incidentView.add(new Label("Incident Category: "), 0, 3);
        incidentView.add(labelType, 1, 3);
        incidentView.add(new Label("Date: "), 0, 4);
        incidentView.add(labelDate, 1, 4);
        incidentView.add(new Label("Time: "), 0, 5);
        incidentView.add(labelTime, 1, 5);
        incidentView.add(new Label("Reported by: "), 0, 6);
        incidentView.add(labelStaff, 1, 6);
        incidentView.add(new Label("Description: "), 0, 7);
        incidentView.add(labelDescription, 1, 7);

        msgBox.getChildren().add(incidentView);
    }

    private void error(){
        msgBox.getChildren().clear();

        msgBox.getChildren().add(new Label("No Incident found. Please try again"));
    }

    private Client getClient(String id){
        Client cl = null;

        try {
            JSONObject jsonResult = httpConnector.postWithParams("getClient.php", "cl_id=" + id);
            JSONArray jsonArray = jsonResult.getJSONArray("client");
            JSONObject jsonClient;
            for(int i = 0; i < jsonArray.length(); i++){
                jsonClient = jsonArray.getJSONObject(i);
                cl = new Client(
                        jsonClient.getInt("cl_id"),
                        jsonClient.getString("cl_name"),
                        jsonClient.getString("cl_dob"),
                        jsonClient.getString("cl_guardian_name"),
                        jsonClient.getString("cl_guardian_contact")
                );
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return cl;
    }
}
