package controller;

import javafx.animation.PauseTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import model.Incident;
import model.Manager;
import model.Staff;
import model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.sql.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class PendingIncidentsController extends Controller implements Initializable {

    @FXML private TableView incidentsTable = new TableView();
    @FXML private GridPane incidentView = new GridPane();
    @FXML private Label labelUserName = new Label();
    @FXML private Button btnHome = new Button();
    @FXML private Button btnIncidents = new Button();

    private ObservableList<Incident> unapprovedIncidentsList = FXCollections.observableArrayList();//a list that contains the student objects

    @Override
    public void initialize(URL location, ResourceBundle resources){
        labelUserName.setText(currentUser.getUsername());

        setupTable();
        setUpTableListener();
        listPendingIncidents();
        setupNavListener(btnHome, "dashboard");
        setupNavListener(btnIncidents, "incidents");
    }

    private void setupTable(){
        TableColumn colIncidentId = new TableColumn("Incident Id");
        TableColumn colDate = new TableColumn("Date");
        TableColumn colTime = new TableColumn("Time");
        TableColumn colType = new TableColumn("Incident Type");
        TableColumn colDesc = new TableColumn("Description");
        TableColumn colStaff = new TableColumn("Staff Id");
        TableColumn colClient = new TableColumn("Client Id");
        TableColumn colStatus = new TableColumn("Status");

        incidentsTable.getColumns().addAll(colIncidentId, colDate, colTime, colType, colDesc, colStaff, colClient, colStatus);

        colIncidentId.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));
        colDate.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.1));
        colTime.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));
        colType.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.1));
        colDesc.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.40));
        colStaff.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));
        colClient.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));
        colStatus.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));

        colIncidentId.setCellValueFactory(new PropertyValueFactory<Incident, String>("incidentId"));
        colDate.setCellValueFactory(new PropertyValueFactory<Incident, String>("incidentDate"));
        colTime.setCellValueFactory(new PropertyValueFactory<Incident, String>("incidentTime"));
        colType.setCellValueFactory(new PropertyValueFactory<Incident, String>("type"));
        colDesc.setCellValueFactory(new PropertyValueFactory<Incident, String>("detail"));
        colStatus.setCellValueFactory(new PropertyValueFactory<Incident, String>("isApproved"));
        colStaff.setCellValueFactory(new PropertyValueFactory<Incident, String>("staffId"));
        colClient.setCellValueFactory(new PropertyValueFactory<Incident, String>("clientId"));

        //root.getChildren().add(incidentsTable);
    }

    private void setUpTableListener(){//double clicking on each row will show the details on the gridpane on bottom
        incidentsTable.setRowFactory( tv -> {
            TableRow<Incident> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Incident rowData = row.getItem();
                    System.out.println("Status is: " + rowData.getIsApproved() + ".");
                    viewIncident(rowData);
                }
            });
            return row ;
        });
    }

    private void viewIncident(Incident in){
        incidentView.getChildren().clear();

        HBox crud = new HBox();
        Button btnDelete = new Button("DELETE");
        Button btnEdit = new Button("EDIT");
        btnDelete.setOnAction(event -> {
            try {
                httpConnector.postNoJSON("deleteIncident.php", "in_id=" + in.getIncidentId());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        btnEdit.setOnAction(event -> {
            editIncident(in);
        });
        crud.getChildren().addAll(btnEdit, btnDelete);

        Label labelType = new Label(in.getType());
        Label labelStatus = new Label(in.getIsApproved());

        Label labelTxtIncidentId = new Label("Incident No: ");
        Label labelIncidentId = new Label(String.valueOf(in.getIncidentId()));

        Label labelIncidentDate = new Label(in.getIncidentDate());
        Label labelIncidentTime = new Label(in.getIncidentTime());

        Label labelTextStaff = new Label("Reported by(client id): ");
        Label labelTextClient = new Label("Client(s) involved: ");

        Label labelStaff = new Label(String.valueOf(in.getStaffId()));
        Label labelClient = new Label(String.valueOf(in.getClientId()));

        Label labelDetail = new Label(in.getDetail());

        Label labelAttention = new Label("This incident needs manager's attention");
        labelAttention.setTextFill(Color.web("ff0000"));

        ComboBox combo = new ComboBox();
        combo.getItems().addAll("Approve", "Unapprove");

        Button btnSave = new Button("Save");

        Label msg = new Label();

        //this block is for making the label visible for few seconds
        PauseTransition visiblePause = new PauseTransition(
                Duration.seconds(4)
        );
        visiblePause.setOnFinished(
                event -> msg.setVisible(false)
        );
        //end

        btnSave.setOnAction(event -> {
            String selection = "";

            if(combo.getValue() == null){
                incidentView.add(new Label("Please select an option"), 1, 7);
            }
            else{
                if(combo.getValue().equals("Approve")){
                    System.out.println("Approve is selected");
                    msg.setText("Incident is successfully updated");
                    selection = "approved";
                }
                else if(combo.getValue().equals("Unapprove")){
                    System.out.println("unApprove is selected");
                    msg.setText("Incident is successfully updated");
                    selection = "unapproved";
                }
                try {
                    httpConnector.postNoJSON("approvalIncident.php", "in_id=" + in.getIncidentId() + "&is_approved=" + selection + "&manager=" + currentUser.getUserId());
                    listPendingIncidents();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                incidentView.getChildren().clear();
                incidentView.getChildren().add(msg);
                visiblePause.play();
            }
        });
        incidentView.add(crud, 0, 0, 2, 1);
        incidentView.add(labelStatus, 0, 1);
        incidentView.add(labelType, 1, 1);
        incidentView.add(labelTxtIncidentId, 0, 2);
        incidentView.add(labelIncidentId, 1, 2);
        incidentView.add(labelIncidentDate, 0, 3);
        incidentView.add(labelIncidentTime, 1, 3);
        incidentView.add(labelTextStaff, 0, 4);
        incidentView.add(labelTextClient, 1, 4);
        incidentView.add(labelStaff, 0, 5);
        incidentView.add(labelClient, 1, 5);
        incidentView.add(labelDetail, 0, 6, 2, 1);

        if(in.getIsApproved().equals("pending")){
            incidentView.add(labelAttention, 0, 7, 2, 1);
            incidentView.add(combo, 0, 8);
            incidentView.add(btnSave, 0, 9);
        }
        incidentView.setVgap(20);
        incidentView.setHgap(20);
    }

    private void editIncident(Incident in){
        incidentView.getChildren().clear();

        Label heading = new Label("UPDATE INCIDENT");

        Label labelTxtIncidentId = new Label("Incident No: " + String.valueOf(in.getIncidentId()));

        TextField tfType = new TextField();
        tfType.setPromptText(in.getType());

        Label labelStatus = new Label("This incident is: " + in.getIsApproved());

        DatePicker dpIncidentDate = new DatePicker();
        dpIncidentDate.setPromptText(in.getIncidentDate());

        TextField tfIncidentTime = new TextField();
        tfIncidentTime.setPromptText(in.getIncidentTime());

        Label labelStaff = new Label("Reported by(client id): " + String.valueOf(in.getStaffId()));
        Label labelClient = new Label("Client(s) involved: " + String.valueOf(in.getClientId()));

        TextArea taDetail = new TextArea(in.getDetail());

        Button btnUpdate = new Button("Update");

        Label msg = new Label();

        //this block is for making the label visible for few seconds
        PauseTransition visiblePause = new PauseTransition(
                Duration.seconds(4)
        );
        visiblePause.setOnFinished(
                event -> msg.setVisible(false)
        );
        //end

        btnUpdate.setOnAction(event -> {
            String selection = "";

            if(tfType.getText().equals("") || dpIncidentDate.getValue() == null || tfIncidentTime.getText().equals("") || taDetail.getText().equals("")){
                incidentView.add(new Label("Please provide data in all fields"), 0, 10);
            }
            else{
                try {
                    httpConnector.postNoJSON("updateIncident.php", "in_id=" + in.getIncidentId() +
                            "&in_date=" + dpIncidentDate.getValue().toString() +
                            "&in_time=" + tfIncidentTime.getText() +
                            "&in_type" + tfType.getText() +
                            "&in_detail" + taDetail.getText() +
                            "&manager" + currentUser.getUserId()
                    );
                    msg.setText("update successful");
                    listPendingIncidents();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                incidentView.getChildren().clear();
                incidentView.getChildren().add(msg);
                visiblePause.play();
            }
        });
        incidentView.add(heading, 0, 0);
        incidentView.add(labelTxtIncidentId, 0, 1);
        incidentView.add(tfType, 0, 2);
        incidentView.add(labelStatus, 0, 3);
        incidentView.add(dpIncidentDate, 0, 4);
        incidentView.add(tfIncidentTime, 0, 5);
        incidentView.add(labelStaff, 0, 6);
        incidentView.add(labelClient, 0, 7);
        incidentView.add(taDetail, 0, 8);
        incidentView.add(btnUpdate, 0, 9);

        incidentView.setVgap(20);
        incidentView.setHgap(20);

    }

    private void listPendingIncidents(){//adds data and displays on the tableview.
        incidentsTable.getItems().clear();

        JSONObject pendingIncidentsJson = null;
        try {
            pendingIncidentsJson = httpConnector.post("getPendingIncidents.php");

            JSONArray jsonArray = pendingIncidentsJson.getJSONArray("pendingIncidents");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject j = jsonArray.getJSONObject(i);
                Incident in = new Incident(
                        j.getInt("in_id"),
                        j.getString("in_time"),
                        j.getString("in_date"),
                        j.getString("in_type"),
                        j.getString("in_detail"),
                        j.getString("is_approved"),
                        j.getString("client"),
                        j.getString("staff"),
                        j.getString("manager"));
                unapprovedIncidentsList.add(in);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        incidentsTable.setItems(unapprovedIncidentsList);//list is added to TableView
    }

}
