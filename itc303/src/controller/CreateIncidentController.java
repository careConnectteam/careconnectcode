package controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CreateIncidentController extends Controller{
    @FXML Label labelUserName = new Label();
    @FXML Label labelLogOut = new Label();
    @FXML AnchorPane canvas = new AnchorPane();
    @FXML Button btnHome = new Button();
    @FXML Button btnIncident = new Button();

    @FXML DatePicker dpDate = new DatePicker();
    @FXML TextField tfInTime = new TextField();
    @FXML TextField tfInType = new TextField();
    @FXML ComboBox<String> cbClient = new ComboBox<>();
    @FXML TextField tfInDetail = new TextField();
    @FXML Button btnCreate = new Button("CREATE");

    @FXML Label err = new Label("ERROR! please check data you entered");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        labelUserName.setText(currentUser.getUsername());
        try {
            setupForm();
        } catch (Exception e) {
            e.printStackTrace();
        }

        setupNavListener(btnHome, "dashboard");
        setupNavListener(btnIncident, "incidents");
        setupNavListener(labelLogOut, "login");
    }

    private void setupForm() throws Exception {
        canvas.getChildren().clear();

        canvas.getChildren().addAll(dpDate, tfInTime, tfInType, cbClient, tfInDetail, btnCreate);

        ArrayList<String> ids = loadClientId();
        for(int i = 0; i<(ids.size()); i++){
            cbClient.getItems().add(ids.get(i));
        }

        btnCreate.setOnAction(event -> {
            if(insert(dpDate.getValue().toString(), tfInTime.getText(), tfInType.getText(), currentUser.getUserId(), cbClient.getValue(), tfInDetail.getText())){
                confirmation();
            }
        });
    }

    private ArrayList<String> loadClientId() throws Exception {
        ArrayList<String> ids = new ArrayList<>();

        JSONObject clientsJson = httpConnector.post("getClients.php");

        JSONArray jsonArray = clientsJson.getJSONArray("clients");

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject j = jsonArray.getJSONObject(i);
            ids.add(j.getString("cl_id"));
        }
        return ids;
    }

    private boolean insert(String date, String time, String type, String staff, String client, String details){
        boolean bool = false;
        String params = "in_date=" + date + "&in_time=" + time + "&type=" + type + "&staff=" +  staff + "&client=" + client + "&detail=" + details;
        try {
            JSONObject res = httpConnector.postWithParams("createIncident.php", params);
            String b = res.get("bool").toString();

            if(b.equals("1")){
                bool = true;
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return bool;
    }

    private void confirmation(){
        canvas.getChildren().clear();

        Label alert = new Label("Incident successfully created");
        Label msg = new Label("to create another incident report press the button below:");
        Button btnForm = new Button("CREATE ANOTHER");

        btnForm.setOnAction(event -> {
            try {
                setupForm();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        canvas.getChildren().addAll(alert, msg, btnForm);
    }

}
