package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class ClientsController extends Controller {

    @FXML Label labelUserName = new Label();
    @FXML TextField tfSearch = new TextField();

    @FXML Button btnSearch = new Button();
    @FXML Button btnViewClients = new Button();
    @FXML Button btnCreateClient = new Button();
    @FXML Button btnHome = new Button();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnHome.setOnAction(event -> {
            goToDashboard(btnHome);
        });

        setupNavListener(btnViewClients, "viewClients");
        setupNavListener(btnCreateClient, "createClient");

        btnSearch.setOnAction(event -> {
            if(tfSearch.getText() == null){
                System.out.println("Please enter something");
            }else{
                goToScreen(btnSearch, "clientSearchResults");
            }

        });
    }
}
