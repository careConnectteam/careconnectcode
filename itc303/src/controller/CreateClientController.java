package controller;

import javafx.animation.PauseTransition;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CreateClientController extends Controller {

    @FXML Label labelUserName = new Label();
    @FXML VBox canvas = new VBox();
    @FXML Button btnHome = new Button();
    @FXML Button btnClients = new Button();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        canvas.setAlignment(Pos.CENTER);
        try {
            setupForm();
        } catch (Exception e) {
            e.printStackTrace();
        }
        btnHome.setOnAction(event -> goToDashboard(btnHome));//takes to dashboard
        btnClients.setOnAction(event -> goToScreen(btnClients, "clients"));//takes to dashboard
    }

    private void setupForm(){
        canvas.getChildren().clear();

        GridPane gpIncidentForm = new GridPane();

        Text msgProvide = new Text("Please provide the details below:");
        Text msgRequired = new Text("All fields are required");

        Label fName = new Label("First Name: ");
        TextField tfFName = new TextField();

        Label lName = new Label("Last: ");
        TextField tfLName = new TextField();

        Label dob = new Label("Date of Birth: ");
        TextField tfDob = new TextField();

        Label gName = new Label("Guardian Name: ");
        TextField tfGName = new TextField();

        Label gContact = new Label("Guardian Contact: ");
        TextField tfGContact = new TextField();

        Button btnCreate = new Button("CREATE");

        Label err = new Label("ERROR! please check data you entered");

        //this block is for making the label visible for few seconds
        PauseTransition visiblePause = new PauseTransition(
                Duration.seconds(4)
        );
        visiblePause.setOnFinished(
                event -> err.setVisible(false)
        );
        //end

        gpIncidentForm.add(msgProvide, 0, 0, 2, 2);
        gpIncidentForm.add(msgRequired, 0, 1, 2, 2);
        gpIncidentForm.add(fName, 0, 3);
        gpIncidentForm.add(tfFName, 1, 3);
        gpIncidentForm.add(lName, 0, 4);
        gpIncidentForm.add(tfLName, 1, 4);
        gpIncidentForm.add(dob, 0, 5);
        gpIncidentForm.add(tfDob, 1, 5);
        gpIncidentForm.add(gName, 0, 6);
        gpIncidentForm.add(tfGName, 1, 6);
        gpIncidentForm.add(gContact, 0, 7);
        gpIncidentForm.add(tfGContact, 1, 7);
        gpIncidentForm.add(btnCreate, 1, 8);
        //gpIncidentForm.getChildren().add(err);

        gpIncidentForm.setVgap(20);
        gpIncidentForm.setHgap(20);
        gpIncidentForm.setAlignment(Pos.CENTER);

        canvas.getChildren().add(gpIncidentForm);

        btnCreate.setOnAction(event -> {
            if(insert(tfFName.getText(), tfLName.getText(), tfDob.getText(), tfGName.getText(), tfGContact.getText())){
                confirmation();
            }
        });
    }

    private boolean insert(String fName, String lName, String dob, String gName, String gContact){
        boolean bool = false;
        String params = "fname=" + fName + "&lname=" + lName + "&dob=" + dob + "&gname=" +  gName + "&gcontact=" + gContact;
        try {
            JSONObject res = httpConnector.postWithParams("createClient.php", params);
            String b = res.get("bool").toString();

            if(b.equals("1")){
                bool = true;
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return bool;
    }

    private void confirmation(){
        canvas.getChildren().clear();

        Label alert = new Label("Incident successfully created");
        Label msg = new Label("to create another incident report press the button below:");
        Button btnForm = new Button("CREATE ANOTHER");

        btnForm.setOnAction(event -> {
            try {
                setupForm();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        canvas.getChildren().addAll(alert, msg, btnForm);
    }
}
