package model;

public class Incident {
    private int incidentId;
    private String incidentTime;
    private String incidentDate;
    private String type;
    private String detail;
    private String isApproved;

    private String clientId;
    private String staffId;
    private String managerId;

    public Incident() {
    }

    public Incident(int incidentId, String incidentTime, String incidentDate, String type, String detail, String isApproved, String clientId, String staffId, String managerId) {
        this.incidentId = incidentId;
        this.incidentTime = incidentTime;
        this.incidentDate = incidentDate;
        this.type = type;
        this.detail = detail;
        this.isApproved = isApproved;
        this.clientId = clientId;
        this.staffId = staffId;
        this.managerId = managerId;
    }

    public int getIncidentId() {
        return incidentId;
    }

    public String getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(String incidentDate) {
        this.incidentDate = incidentDate;
    }

    public String getIncidentTime() {
        return incidentTime;
    }

    public void setIncidentTime(String incidentTime) {
        this.incidentTime = incidentTime;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public void approve(String managerId){
        if(managerId.equals("") || managerId.equals("null") || managerId.equals("NULL")){
            setManagerId(managerId);
            setIsApproved(Status.approved.toString());
        }
    }
}
