package model;

public class Note {
    private int noteId;
    private String type;
    private String date;
    private String time;
    private String description;
    private int staffId;
    private int clientId;

    public Note(int noteId, String type, String date, String time, String description, int staffId, int clientId) {
        this.noteId = noteId;
        this.type = type;
        this.date = date;
        this.time = time;
        this.description = description;
        this.staffId = staffId;
        this.clientId = clientId;
    }

    public int getNoteId() {
        return noteId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
