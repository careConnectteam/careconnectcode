package model;

public class Client {
    private int clientId;
    private String name;
    private String dob;
    private String guardianName;
    private String guardianContact;

    public Client(int clientId, String name, String dob, String guardianName, String guardianContact) {
        this.clientId = clientId;
        this.name = name;
        this.dob = dob;
        this.guardianName = guardianName;
        this.guardianContact = guardianContact;
    }

    public int getClientId() {
        return clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGuardianName() {
        return guardianName;
    }

    public void setGuardianName(String guardianName) {
        this.guardianName = guardianName;
    }

    public String getGuardianContact() {
        return guardianContact;
    }

    public void setGuardianContact(String guardianContact) {
        this.guardianContact = guardianContact;
    }
}
