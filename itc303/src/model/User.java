package model;

public abstract class User {
    private String userId;
    private String name;
    private String contact;
    private String address;
    private String type;
    private String username;

    public User() {

    }

    public User(String userId, String name, String contact, String address, String type, String username) {
        this.userId = userId;
        this.name = name;
        this.contact = contact;
        this.address = address;
        this.type = type;
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
